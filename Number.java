
public class Number extends Operation {
	
	private char datum;
	
	public Number(char datum) {
		
		super(null, null);
		
		this.datum = datum;
		
	}
	
	public char getDatum() {
		
		return datum;
		
	}

	@Override
	public int eval() {
		
		return datum - '0';
		
	}
	
	public void setLeft(Operation newLeft) {
		
	}

	public void setRight(Operation newRight) {

	}
	
	@Override
	public Operation getLeft() {
		
		return null;
	}

	@Override
	public Operation getRight() {
		
		return null;
		
	}
	
	public boolean isLeaf() {

		return true;
	}

}
