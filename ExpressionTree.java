
public class ExpressionTree {

	/** Stack which used to store TreeCell (non-OOP) for makeExpTree method. */
	private static Stack <TreeCell<Character>> stack = new ListStack <TreeCell<Character>> ();

	/** Stack which used to store TreeCell (OOP) for makeExpTree method. */
	private static Stack<Operation> stackOOP = new ListStack<Operation>();

	/** Stack which used to store character for intoPos method. */
	private static Stack <Character> operators = new ListStack <Character> ();

	/**
	 * Convert infix-expression to postfix-expression.
	 * @param infix as an input
	 * @return converted postfix-expression
	 */
	public static String intoPos(String infix) {

		// Base case : end of input
		
		if(infix.length() == 0) {

			String temp = "";

			// if stack is not empty, then pop until it's empty.
			
			while(!operators.isEmpty()) {

				temp += operators.pop();

			}

			return temp;

		}

		// if character is digit, then output it.
		
		if(Character.isDigit(infix.charAt(0))) {

			return infix.charAt(0) + intoPos(infix.substring(1));

		}

		// if character is open-parenthesis, then push it into stack.
		
		else if(infix.charAt(0) == '(') {

			operators.push('(');

			return intoPos(infix.substring(1));
		}

		// if character is close-parenthesis, then pop until open-parenthesis is detected.
		
		else if(infix.charAt(0) == ')') {

			String temp = "";

			while(!operators.isEmpty() && operators.peek() != '(') {

				temp += operators.pop();

			}

			operators.pop();

			return temp + intoPos(infix.substring(1));

		}

		// if character is multiply or divide and top of stack (TOS) is multiply or divide, then pop into output and push current into stack.
		
		else if((infix.charAt(0) == '*' || infix.charAt(0) == '/') && !operators.isEmpty() && (operators.peek() == '*' || operators.peek() == '/')) {

			char temp = operators.pop();

			operators.push(infix.charAt(0));

			return temp + intoPos(infix.substring(1));
		}

		// if character is multiply or divide and top of stack (TOS) is not multiply or divide, then push current into stack.
		
		else if(infix.charAt(0) == '*' || infix.charAt(0) == '/') {

			operators.push(infix.charAt(0));

			return intoPos(infix.substring(1));

		}

		// if character is add or minus and top of stack (TOS) is multiply or divide, then pop until open-parenthesis is detected and push current into stack.
		
		else if((infix.charAt(0) == '+' || infix.charAt(0) == '-') && !operators.isEmpty() && (operators.peek() == '*' || operators.peek() == '/')) {

			String temp = "";

			while(!operators.isEmpty() && operators.peek() != '(') {

				temp += operators.pop();

			}

			operators.push(infix.charAt(0));

			return temp + intoPos(infix.substring(1));

		}

		// if character is add or minus and top of stack (TOS) is add or minus, then pop into output and push current into stack.
		
		else if((infix.charAt(0) == '+' || infix.charAt(0) == '-') && !operators.isEmpty() && (operators.peek() == '-' || operators.peek() == '+')) {

			char temp = operators.pop();

			operators.push(infix.charAt(0));

			return temp + intoPos(infix.substring(1));
		}

		// if character is add or minus and top of stack (TOS) is not add or minus, then push current into stack.
		
		else if(infix.charAt(0) == '+' || infix.charAt(0) == '-') {

			operators.push(infix.charAt(0));

			return intoPos(infix.substring(1));
		}

		else {

			return intoPos(infix.substring(1));
		}

	}

	/* This method returns a root to the equivalent expression tree of the input postfix form. */

	public static TreeCell<Character> makeExpTree(String postfix) {

		if(postfix.length() == 0) return stack.pop(); // Base case : end of input

		TreeCell<Character> cell = new TreeCell<Character>(postfix.charAt(0));

		// if cell kind is operator, pop stack and set right then pop stack again and set left.
		
		// Otherwise, do nothing.
		
		if(cell.getKind() == Kind.OPERATOR) {

			cell.setRight(stack.pop());

			cell.setLeft(stack.pop());

		}

		// Push it into stack.
		
		stack.push(cell);

		return makeExpTree(postfix.substring(1));

	}

	/* This method returns a root to the equivalent expression tree of the input postfix form (OOP). */
	
	public static Operation makeExpTreeOOP(String postfix) {

		if(postfix.length() == 0) return stackOOP.pop(); // Base case : end of input

		Operation cell;
		
		// Create operation depending on operators.

		switch(postfix.charAt(0)) {

		case '+' :

			cell = new PlusOperation(null, null);

			cell.setRight(stackOOP.pop());

			cell.setLeft(stackOOP.pop());

			break;

		case '-' :

			cell = new MinusOperation(null, null);

			cell.setRight(stackOOP.pop());

			cell.setLeft(stackOOP.pop());

			break;

		case '*' :

			cell = new MultiplyOperation(null, null);

			cell.setRight(stackOOP.pop());

			cell.setLeft(stackOOP.pop());

			break;

		case '/' :

			cell = new DivideOperation(null, null);

			cell.setRight(stackOOP.pop());

			cell.setLeft(stackOOP.pop());

			break;

		default :

			cell = new Number(postfix.charAt(0));

			break;

		}

		stackOOP.push(cell);

		return makeExpTreeOOP(postfix.substring(1));

	}

	// Print prefix-expression from TreeCell.
	
	public static void printPrefix(TreeCell<?> root) {

		System.out.print(root.getDatum());

		if(root.isLeaf()) return;

		printPrefix(root.getLeft());

		printPrefix(root.getRight());

	}
	
	// Print infix-expression from TreeCell.
	
		public static void printInfix(TreeCell<Character> root) {

			if(root.isLeaf()) {

				System.out.print(root.getDatum());

				return;
			}

			printInfix(root.getLeft());

			System.out.print(root.getDatum());

			printInfix(root.getRight());

		}

	// Print postfix-expression from TreeCell.
	
	public static void printPostfix(TreeCell<?> root) {

		if(root.isLeaf()) {

			System.out.print(root.getDatum());

			return;

		}

		printPostfix(root.getLeft());

		printPostfix(root.getRight());

		System.out.print(root.getDatum());

	}

	// Print prefix-expression from Operation (OOP).
	
	public static void printPrefixOOP(Operation root) {

		System.out.print(root.getDatum());

		if(root.isLeaf()) return;

		printPrefixOOP(root.getLeft());

		printPrefixOOP(root.getRight());

	}
	
	// Print infix-expression from Operation (OOP).
	
	public static void printInfixOOP(Operation root) {

		if(root.isLeaf()) {

			System.out.print(root.getDatum());

			return;
		}

		printInfixOOP(root.getLeft());

		System.out.print(root.getDatum());

		printInfixOOP(root.getRight());

	}

	// Print postfix-expression from Operation (OOP).
	
	public static void printPostfixOOP(Operation root) {

		if(root.isLeaf()) {

			System.out.print(root.getDatum());

			return;

		}

		printPostfixOOP(root.getLeft());

		printPostfixOOP(root.getRight());

		System.out.print(root.getDatum());

	}

	public static void printTree(TreeCell<Character> root, int level ) {

		if (root != null) {

			printTree(root.getRight(), level+1);

			for (int i = 0; i < level; i++) System.out.print(" ");

			System.out.println(root.getDatum());

			System.out.println();

			printTree(root.getLeft(), level+1);

		}
	}

	// Evaluate the expression tree.
	
	public static int eval(TreeCell<Character> root) {

		if(root.isLeaf()) return root.getDatum() - '0';

		switch(root.getDatum()) {

		case '+' :

			return eval(root.getLeft()) + eval(root.getRight());

		case '-' :

			return eval(root.getLeft()) - eval(root.getRight());

		case '*' :

			return eval(root.getLeft()) * eval(root.getRight());

		case '/' :

			return eval(root.getLeft()) / eval(root.getRight());

		}

		return root.getDatum();
	}

	public static void main(String[] args) {

		String expression1 = "( 4 * 2 ) + ( 5 - 2 ) + ( 6 + 4 ) * ( 1 + 1 )";

		String expression2 = "3 * ( ( 7 + 1 ) / 4 ) + ( 7 - 5 )";

		String expression3 = "6 + ( ( 4 + ( 3 * 7 ) ) - 6 / ( 4 + 2 ) )";

		String expression4 = "5 * ( 9 - 3 ) / ( 6 / ( 1 * 2 ) )";

		String expression5 = " 2 * 3 / ( 2 - 1 ) + 5 * ( 4 - 1 )";

		Operation tree;
		
		System.out.println();
		
		System.out.println("----- Project 2 Test Cases -----");
		
		System.out.println();

		System.out.println("Case 1 : " + expression1);

		String postfix1 = intoPos(expression1);

		tree = makeExpTreeOOP(postfix1);

		System.out.println();

		System.out.print("Prefix : ");

		printPrefixOOP(tree);

		System.out.println();

		System.out.println();

		System.out.print("Infix : ");

		printInfixOOP(tree);

		System.out.println();

		System.out.println();

		System.out.print("Postfix : ");

		printPostfixOOP(tree);
		
		System.out.println();

		System.out.println();
		
		System.out.println("Evaluation = " + tree.eval());
		
		System.out.println();
		
		System.out.println("-----------------------");
		
		System.out.println();	
		
		System.out.println("Case 2 : " + expression2);

		String postfix2 = intoPos(expression2);

		tree = makeExpTreeOOP(postfix2);

		System.out.println();

		System.out.print("Prefix : ");

		printPrefixOOP(tree);

		System.out.println();

		System.out.println();

		System.out.print("Infix : ");

		printInfixOOP(tree);

		System.out.println();

		System.out.println();

		System.out.print("Postfix : ");

		printPostfixOOP(tree);
		
		System.out.println();

		System.out.println();
		
		System.out.println("Evaluation = " + tree.eval());
		
		System.out.println();
		
		System.out.println("-----------------------");
		
		System.out.println();
		
		System.out.println("Case 3 : " + expression3);

		String postfix3 = intoPos(expression3);
		
		tree = makeExpTreeOOP(postfix3);

		System.out.println();

		System.out.print("Prefix : ");

		printPrefixOOP(tree);

		System.out.println();

		System.out.println();

		System.out.print("Infix : ");

		printInfixOOP(tree);

		System.out.println();

		System.out.println();

		System.out.print("Postfix : ");

		printPostfixOOP(tree);
		
		System.out.println();

		System.out.println();
		
		System.out.println("Evaluation = " + tree.eval());
		
		System.out.println();
		
		System.out.println("-----------------------");
		
		System.out.println();
		
		System.out.println("Case 4 : " + expression4);
		
		String postfix4 = intoPos(expression4);
		
		tree = makeExpTreeOOP(postfix4);

		System.out.println();

		System.out.print("Prefix : ");

		printPrefixOOP(tree);

		System.out.println();

		System.out.println();

		System.out.print("Infix : ");

		printInfixOOP(tree);

		System.out.println();

		System.out.println();

		System.out.print("Postfix : ");

		printPostfixOOP(tree);
		
		System.out.println();

		System.out.println();
		
		System.out.println("Evaluation = " + tree.eval());
		
		System.out.println();
		
		System.out.println("-----------------------");
		
		System.out.println();
		
		System.out.println("Case 5 : " + expression5);

		String postfix5 = intoPos(expression5);
		
		tree = makeExpTreeOOP(postfix5);

		System.out.println();

		System.out.print("Prefix : ");

		printPrefixOOP(tree);

		System.out.println();

		System.out.println();

		System.out.print("Infix : ");

		printInfixOOP(tree);

		System.out.println();

		System.out.println();

		System.out.print("Postfix : ");

		printPostfixOOP(tree);
		
		System.out.println();

		System.out.println();
		
		System.out.println("Evaluation = " + tree.eval());
		
		System.out.println();
		
		System.out.println("-----------------------");
		
		System.out.println();
		
	}
}
