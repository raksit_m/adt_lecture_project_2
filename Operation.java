
public abstract class Operation {
	
	private char datum;
	
	private Operation left, right;
	
	public Operation(Operation left, Operation right) {
		
		this.left = left;
		
		this.right = right;
		
	}
	
	public char getDatum() {
		
		return datum;
		
	}

	public abstract int eval();
	
	public abstract void setLeft(Operation newLeft);

	public abstract void setRight(Operation newRight);
	
	public abstract Operation getLeft();
	
	public abstract Operation getRight();
	
	public abstract boolean isLeaf();

}
