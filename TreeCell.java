
public class TreeCell<T extends Comparable<T>> {

	private T datum;

	private TreeCell<T> left, right;
	
	private Kind kind;
	
//	private Data data; 

	public TreeCell(T x) {

		datum = x;
		
		setKind(datum);

	}

	public TreeCell(T x, TreeCell<T> lft, TreeCell<T> rgt) {

		datum = x;

		left = lft;

		right = rgt;
		
	    setKind(datum);

	}

	public T getDatum() {

		return datum;

	}

	public void setDatum(T newDatum) {

		datum = newDatum;

	}

	public TreeCell<T> getLeft() {

		return left;

	}

	public void setLeft(TreeCell<T> treeCell) {

		left = treeCell;

	}

	public TreeCell<T> getRight() {

		return right;

	}

	public void setRight(TreeCell<T> newRight) {

		right = newRight;

	}
	
	public void setKind(T datum) {
		
		if(Character.isDigit(datum.toString().charAt(0))) {
			
			kind = Kind.NUMBER;
		}
		
		else kind = Kind.OPERATOR;
	}
	
	public Kind getKind() {
		
		return kind;
	}

	public void setKind(Kind kind) {
		
		this.kind = kind;
	}

	public boolean search(T x, TreeCell<T> node) {

		if(node == null) return false;

		if(node.getDatum().equals(x)) return true;

		if(node.getDatum().compareTo(x) > 0)

			return search(x, node.getLeft());

		else return search(x, node.getRight());

	}

	public static void preOrder(TreeCell<?> T) {

		if(T == null) return;

		System.out.print(T.getDatum() + " ");

		preOrder(T.getLeft());

		preOrder(T.getRight());
	}

	public static void inOrder(TreeCell<?> T) {

		if(T == null) return;

		inOrder(T.getLeft());

		System.out.print(T.getDatum() + " ");

		inOrder(T.getRight());
	}

	public void postOrder(TreeCell<?> T) {

		if(T == null) return;

		postOrder(T.getLeft());

		postOrder(T.getRight());

		System.out.print(T.getDatum() + " ");

	}

	public TreeCell<T> insert(T x, TreeCell<T> T) {

		if (T == null) return new TreeCell<T>(x);

		else if (T.getDatum().compareTo(x) > 0) {

			T.setLeft(insert(x, T.getLeft()));

		}

		else {

			T.setRight(insert(x, T.getRight()));

		}

		return T;
	}
	
	public boolean isLeaf() {
		
		return left == null && right == null;
		
	}

	// returns a reference to the root of binary search tree T with node x (the datum) removed

	public TreeCell<T> delete(T value, TreeCell<T> root) {

		if(!search(value, root)) return root;

		if(root.getLeft() == null && root.getRight() == null) {

			return null;
		}

		if(value.compareTo(root.getDatum()) < 0) {

			root.setLeft(delete(value, root.getLeft()));

		}

		else if(value.compareTo(root.getDatum()) > 0) {

			root.setRight(delete(value, root.getRight()));

		}

		else {

			if(root.getRight() != null) {

				TreeCell<T> temp = findMinimumRight(root.getRight());

				root.setDatum(temp.getDatum());

				root.setRight(delete(temp.getDatum(), root.getRight()));
			}

			else {

				TreeCell<T> temp = findMaximumLeft(root.getLeft());

				root.setDatum(temp.getDatum());

				root.setLeft(delete(temp.getDatum(), root.getLeft()));

			}
		}

		return root;
	}

	public TreeCell<T> findMinimumRight(TreeCell<T> treeCell) {

		if(treeCell.getLeft() == null) return treeCell;

		else return findMinimumRight(treeCell.getLeft());
	}

	public TreeCell<T> findMaximumLeft(TreeCell<T> treeCell) {

		if(treeCell.getRight() == null) return treeCell;

		else return findMaximumLeft(treeCell.getRight());
	}

	public TreeCell<T> removeDuplicates(TreeCell<T> treeCell) {

		if(treeCell.getLeft() == null) return null;

		treeCell.setLeft(removeDuplicates(treeCell.getLeft()));

		return treeCell;
	}
}
