
public class PlusOperation extends Operation {

	private char datum;

	private Operation left; 

	private Operation right;

	public PlusOperation(Operation left, Operation right) {

		super(left, right);

		datum = '+';

	}

	public char getDatum() {

		return datum;

	}

	@Override
	public int eval() {

		return left.eval() + right.eval();

	}
	
	public void setLeft(Operation newLeft) {

		left = newLeft;

	}

	public void setRight(Operation newRight) {

		right = newRight;

	}

	@Override
	public Operation getLeft() {

		return left;
	}

	@Override
	public Operation getRight() {

		return right;

	}

	public boolean isLeaf() {

		return false;
	}

}
